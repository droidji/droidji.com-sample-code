package com.droidji.calculatordemo;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity implements View.OnClickListener
{

    private double mCurrentValue;
    private double mStoredValue;
    private int mFractionDivisionValue;
    private Operation mCurrentOperation;
    private boolean mEqualsPressed;

    enum Operation
    {
        ADD,
        SUBTRACT,
        MULTIPLY,
        DIVIDE,
        INVALID
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init()
    {
        clear();
        setClickListeners();
    }

    private void setClickListeners()
    {
        TextView result = (TextView)findViewById(R.id.text_result);
        result.setText(getResources().getText(R.string.zero_point_zero));

        Button button = (Button) findViewById(R.id.button_clear);
        button.setOnClickListener(this);

        button = (Button) findViewById(R.id.button_0);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_1);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_2);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_3);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_4);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_5);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_6);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_7);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_8);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_9);
        button.setOnClickListener(this);

        button = (Button) findViewById(R.id.button_divide);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_multiply);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_subtract);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_add);
        button.setOnClickListener(this);

        button = (Button) findViewById(R.id.button_dot);
        button.setOnClickListener(this);
        button = (Button) findViewById(R.id.button_equals);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.button_clear:
                clear();
                break;
            case R.id.button_0:
                processNumberButtonClick(0);
                break;
            case R.id.button_1:
                processNumberButtonClick(1);
                break;
            case R.id.button_2:
                processNumberButtonClick(2);
                break;
            case R.id.button_3:
                processNumberButtonClick(3);
                break;
            case R.id.button_4:
                processNumberButtonClick(4);
                break;
            case R.id.button_5:
                processNumberButtonClick(5);
                break;
            case R.id.button_6:
                processNumberButtonClick(6);
                break;
            case R.id.button_7:
                processNumberButtonClick(7);
                break;
            case R.id.button_8:
                processNumberButtonClick(8);
                break;
            case R.id.button_9:
                processNumberButtonClick(9);
                break;
            case R.id.button_divide:
                processOperationStart(Operation.DIVIDE);
                break;
            case R.id.button_multiply:
                processOperationStart(Operation.MULTIPLY);
                break;
            case R.id.button_subtract:
                processOperationStart(Operation.SUBTRACT);
                break;
            case R.id.button_add:
                processOperationStart(Operation.ADD);
                break;
            case R.id.button_dot:
                processDotButtonClick();
                break;
            case R.id.button_equals:
                processEqualsButtonClick();
                break;
        }
    }

    private void processOperationStart(Operation operation)
    {
        mStoredValue = mCurrentValue;
        mCurrentValue = 0;
        mCurrentOperation = operation;
        mEqualsPressed = false;
        mFractionDivisionValue = 0;
        updateResultField();
    }

    private void processEqualsButtonClick()
    {
        switch (mCurrentOperation)
        {
            case ADD:
                mCurrentValue = mStoredValue + mCurrentValue;
                break;
            case SUBTRACT:
                mCurrentValue = mStoredValue - mCurrentValue;
                break;
            case DIVIDE:
                mCurrentValue = mStoredValue / mCurrentValue;
                break;
            case MULTIPLY:
                mCurrentValue = mStoredValue * mCurrentValue;
            case INVALID:
                 //nothing to do
        }
        mEqualsPressed = true;
        mCurrentOperation = Operation.INVALID;
        updateResultField();
    }

    private void updateResultField()
    {
        TextView result = (TextView)findViewById(R.id.text_result);
        result.setText(Double.toString(mCurrentValue));
    }

    private void processDotButtonClick()
    {
        if (0 == mFractionDivisionValue && false == mEqualsPressed)
        {
            mFractionDivisionValue = 10;
        }
    }

    private void processNumberButtonClick(int number)
    {
        if (true == mEqualsPressed)
        {
            mCurrentValue = 0;
            mEqualsPressed = false;
            mFractionDivisionValue = 0;
        }
        if (mFractionDivisionValue > 0)
        {
            mCurrentValue = mCurrentValue + ((double)number / (double)mFractionDivisionValue);
            mFractionDivisionValue *= 10;
        }
        else
        {
            mCurrentValue = (mCurrentValue * 10) + number;
        }
        updateResultField();
    }

    private void clear()
    {
        mEqualsPressed = false;
        mCurrentValue = 0;
        mStoredValue = 0;
        mFractionDivisionValue = 0;
        mCurrentOperation = Operation.INVALID;
        updateResultField();
    }
}
